using Kamikazi.Contracts.Reservation;
using Kamikazi.Models;
using Kamikazi.ServiceErrors;
using Kamikazi.Services.Reservations;
using ErrorOr;
using Microsoft.AspNetCore.Mvc;

namespace Kamikazi.Controllers;

public class ReservationsController : ApiController
{
    private readonly IReservationService _reservationService;

    public ReservationsController(IReservationService reservationService)
    {
        _reservationService = reservationService;
    }

    [HttpPost]
    public IActionResult CreateReservation(MakeReservationRequest request)
    {
        ErrorOr<Reservation> requestToReservationResult = Reservation.From(request);

        if (requestToReservationResult.IsError)
        {
            return Problem(requestToReservationResult.Errors);
        }

        var reservation = requestToReservationResult.Value;
        ErrorOr<Created> createReservationResult = _reservationService.CreateReservation(reservation);

        return createReservationResult.Match(
            created => CreatedAtGetReservation(reservation),
            errors => Problem(errors));
    }

    [HttpGet("{id:guid}")]
    public IActionResult GetReservation(Guid id)
    {
        ErrorOr<Reservation> getReservationResult = _reservationService.GetReservation(id);

        return getReservationResult.Match(
            reservation => Ok(MapReservationResponse(reservation)),
            errors => Problem(errors));
    }

    [HttpDelete("{id:guid}")]
    public IActionResult DeleteReservation(Guid id)
    {
        ErrorOr<Deleted> deleteReservationResult = _reservationService.DeleteReservation(id);

        return deleteReservationResult.Match(
            deleted => NoContent(),
            errors => Problem(errors));
    }

    private static ReservationResponse MapReservationResponse(Reservation reservation)
    {
        return new ReservationResponse(
            reservation.Id,
            reservation.TableNumber,
            reservation.Email,
            reservation.Description,
            reservation.StartDateTime,
            reservation.EndDateTime,
            reservation.LastModifiedDateTime);
    }

    private CreatedAtActionResult CreatedAtGetReservation(Reservation reservation)
    {
        return CreatedAtAction(
            actionName: nameof(GetReservation),
            routeValues: new { id = reservation.Id },
            value: MapReservationResponse(reservation));
    }
}
