using Microsoft.AspNetCore.Mvc;

namespace Kamikazi.Controllers;

public class ErrorsController : ControllerBase
{
    [Route("/error")]
    public IActionResult Error()
    {
        return Problem();
    }
}
