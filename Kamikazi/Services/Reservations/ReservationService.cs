using Kamikazi.Models;
using Kamikazi.ServiceErrors;
using ErrorOr;
using Kamikazi.Persistence;

namespace Kamikazi.Services.Reservations;

public class ReservationService : IReservationService
{
    private readonly ReservationDbContext _dbContext;

    public ReservationService(ReservationDbContext dbContext){
        _dbContext = dbContext;
    }

    public ErrorOr<Created> CreateReservation(Reservation reservation)
    {
        _dbContext.Reservations.Add(reservation);
        _dbContext.SaveChanges();

        return Result.Created;
    }

    public ErrorOr<Deleted> DeleteReservation(Guid id)
    {
        var reservation = _dbContext.Reservations.Find(id);
        if (reservation is null){
            return Errors.Reservation.NotFound;
        }
        _dbContext.Reservations.Remove(reservation);
        _dbContext.SaveChanges();

        return Result.Deleted;
    }

    public ErrorOr<Reservation> GetReservation(Guid id)
    {
        if (_dbContext.Reservations.Find(id) is Reservation reservation)
        {
            return reservation;
        }

        return Errors.Reservation.NotFound;
    }


}
