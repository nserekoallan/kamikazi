using Kamikazi.Models;
using ErrorOr;

namespace Kamikazi.Services.Reservations;

public interface IReservationService
{
    ErrorOr<Created> CreateReservation(Reservation reservation);
    ErrorOr<Reservation> GetReservation(Guid id);
    ErrorOr<Deleted> DeleteReservation(Guid id);
}
