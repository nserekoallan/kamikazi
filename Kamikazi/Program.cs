using Kamikazi.Persistence;
using Kamikazi.Services.Reservations;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
{
    builder.Services.AddControllers();
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
    builder.Services.AddScoped<IReservationService, ReservationService>();
    builder.Services.AddDbContext<ReservationDbContext>(options => options.UseInMemoryDatabase("reservations"));

     /*For SQLite*/
    // var connectionString = builder.Configuration.GetConnectionString("reservations") ?? "Data Source=Reservation.db";
}


var app = builder.Build();
app.UseExceptionHandler("/error");
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
