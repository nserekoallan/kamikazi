using ErrorOr;

namespace Kamikazi.ServiceErrors;

public static class Errors
{
    public static class Reservation
    {
        public static Error InvalidTableNumber => Error.Validation(
            code: "Reservation.InvalidTableNumber",
            description: $"Reservation table number must between {Models.Reservation.MinTableNumber}" +
                $" and {Models.Reservation.MaxTableNumber} characters long.");

        public static Error InvalidDescription => Error.Validation(
            code: "Reservation.InvalidDescription",
            description: $"Reservation description must be at least {Models.Reservation.MinDescriptionLength}" +
                $" characters long and at most {Models.Reservation.MaxDescriptionLength} characters long.");

        public static Error NotFound => Error.NotFound(
            code: "Reservation.NotFound",
            description: "Reservation not found");
    }
}
