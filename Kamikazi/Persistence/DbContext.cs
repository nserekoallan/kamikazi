
using Kamikazi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Kamikazi.Persistence;

public class ReservationDbContext: DbContext{
    public ReservationDbContext(DbContextOptions<ReservationDbContext> options) : base(options){}

    public DbSet<Reservation> Reservations {get; set; } = null!;

}
