using Kamikazi.Contracts.Reservation;
using Kamikazi.ServiceErrors;
using ErrorOr;

namespace Kamikazi.Models;

public class Reservation
{
    public const int MinTableNumber = 1;
    public const int MaxTableNumber= 10;

    public const int MinDescriptionLength = 10;
    public const int MaxDescriptionLength = 70;

    public Guid Id { get; set;}
    public string Email { get; set;}

    public int TableNumber { get; set; }

    public string Description { get; set;}
    public DateTime StartDateTime { get; set;}
    public DateTime EndDateTime { get; set;}
    public DateTime LastModifiedDateTime { get; set;}

    private Reservation(
        Guid id,
        string email,
        int tableNumber,
        string description,
        DateTime startDateTime,
        DateTime endDateTime,
        DateTime lastModifiedDateTime)
    {
        Id = id;
        Email = email;
        TableNumber = tableNumber;
        Description = description;
        StartDateTime = startDateTime;
        EndDateTime = endDateTime;
        LastModifiedDateTime = lastModifiedDateTime;
    }

    public static ErrorOr<Reservation> Create(
        string email,
        int tableNumber,
        string description,
        DateTime startDateTime,
        DateTime endDateTime,
        Guid? id = null)
    {
        List<Error> errors = new();

        if (tableNumber is < MinTableNumber or > MaxTableNumber)
        {
            errors.Add(Errors.Reservation.InvalidTableNumber);
        }

        if (description.Length is < MinDescriptionLength or > MaxDescriptionLength)
        {
            errors.Add(Errors.Reservation.InvalidDescription);
        }

        if (errors.Count > 0)
        {
            return errors;
        }

        return new Reservation(
            id ?? Guid.NewGuid(),
            email,
            tableNumber,
            description,
            startDateTime,
            endDateTime,
            DateTime.UtcNow);
    }

    public static ErrorOr<Reservation> From(MakeReservationRequest request)
    {
        return Create(
            request.Email,
            request.TableNumber,
            request.Description,
            request.StartDateTime,
            request.EndDateTime,
            Guid.NewGuid());
    }

}
