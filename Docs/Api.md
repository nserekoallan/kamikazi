# Kamikazi Core API

- [Kamikazi Core API](#kamikazi-core-api)
  - [Make Reservation](#make-reservation)
    - [Make Reservation Request](#make-reservation-request)
    - [Make Reservation Response](#make-reservation-response)
  - [Get Reservation](#get-reservation)
    - [Get Reservation Request](#get-reservation-request)
    - [Get Reservation Response](#get-reservation-response)
  - [Cancel Reservation](#delete-reservation)
    - [Cancel Reservation Request](#delete-reservation-request)
    - [Cancel Reservation Response](#delete-reservation-response)

## Make Reservation

### Make Reservation Request

```js
POST /reservations
```

```json
{
    "tableNumber": 4,
    "email": "nserekoallan96@gmail.com",
    "description": "Table for 4",
    "startDateTime": "2022-04-08T08:00:00",
    "endDateTime": "2022-04-08T11:00:00",
}
```

### Make Reservation Response

```js
201 Created
```

```yml
Location: {{host}}/Reservations/{{id}}
```

```json
{
    "id": "00000000-0000-0000-0000-000000000000",
    "tableNumber": 4,
    "email": "nserekoallan96@gmail.com",
    "description": "Table for 4",
    "startDateTime": "2022-04-08T08:00:00",
    "endDateTime": "2022-04-08T11:00:00",
    "lastModifiedDateTime": "2022-04-06T12:00:00",
}
```

## Get Reservation

### Get Reservation Request

```js
GET /reservations/{{id}}
```

### Get Reservation Response

```js
200 Ok
```

```json
{
    "id": "00000000-0000-0000-0000-000000000000",
    "tableNumber": 4,
    "email": "nserekoallan96@gmail.com",
    "description": "Table for 4",
    "startDateTime": "2022-04-08T08:00:00",
    "endDateTime": "2022-04-08T11:00:00",
    "lastModifiedDateTime": "2022-04-06T12:00:00",
}
```

## Cancel Reservation

### Cancel Reservation Request

```js
DELETE /reservations/{{id}}
```

### Cancel Reservation Response

```js
204 No Content
```
