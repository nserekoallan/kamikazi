namespace Kamikazi.Contracts.Reservation;

public record ReservationResponse(
    Guid Id,
    int TableNumber,
    string Email,
    string Description,
    DateTime StartDateTime,
    DateTime EndDateTime,
    DateTime LastModifiedDateTime
);
