namespace Kamikazi.Contracts.Reservation;

public record MakeReservationRequest(
    string Email,
    string Description,
    int TableNumber,
    DateTime StartDateTime,
    DateTime EndDateTime);
